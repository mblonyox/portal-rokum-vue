import store from '../store'

export const auth = (to, from, next) => {
  if(store.getters.isLoggedIn) return next()
  return next('/login')
}

export const guest = (to, from, next) => {
  if(!store.getters.isLoggedIn) return next()
  return next(false)
}

export const role = (roles) => (to, from, next) => {
  if(roles.some(r => ~store.state.auth.roles.indexOf(r))) return next()
  return next('/forbidden')
}