export default function(name, isFolder = false) {
  return () => import(`@/views/${name}${isFolder ? '/index' : ''}.vue`)
}
