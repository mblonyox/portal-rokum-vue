import Vue from 'vue'
import Router from 'vue-router'

import lazyLoad from './lazyLoad'
import {auth as authGuard, guest as guestGuard} from './guards'

import MainLayout from '@/layouts/MainLayout'

import NotFound from '@/views/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      component: MainLayout,
      children: [
        {
          path: '',
          name: 'home',
          component: lazyLoad('Home')
        },
        {
          path: 'dashboard',
          name: 'dashboard',
          component: lazyLoad('Dashboard')
        },
        {
          path: 'profile',
          name: 'profile',
          component: lazyLoad('Profile')
        },
        {
          path: 'naskah-keluar',
          name: 'naskah-keluar',
          component: lazyLoad('TataNaskahDinas/NaskahKeluarDaftar')
        }
      ],
      beforeEnter: authGuard
    },
    {
      path: '/about',
      name: 'about',
      component: lazyLoad('About')
    },
    {
      path: '/login',
      name: 'login',
      component: lazyLoad('Login'),
      beforeEnter: guestGuard
    },
    {
      path: '/404',
      component: NotFound
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})
