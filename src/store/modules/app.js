import NProgress from 'nprogress'

const sidebarMenus = [
  {
    name: 'Beranda',
    icon: 'home',
    path: '/'
  },
  {
    name: 'Dashboard',
    icon: 'dashboard',
    path: '/dashboard',
    allow: ['rokum']
  },
  {
    isGroup: true,
    name: 'Tata Naskah Dinas',
    icon: 'envelope',
    allow: ['rokum'],
    submenus: [
      {
        name: 'Naskah Dinas Masuk',
        icon: 'inbox',
        path: '/naskah-masuk'
      },
      {
        name: 'Naskah Dinas Keluar',
        icon: 'file-text-o',
        path: '/naskah-keluar'
      },
      {
        name: 'Verbal Naskah Dinas',
        icon: 'files-o',
        path: '/verbal'
      }
    ]
  },
  {
    isGroup: true,
    name: 'Kearsipan',
    icon: 'folder-open-o',
    allow: ['rokum'],
    submenus: [
      {
        name: 'Daftar Arsip',
        icon: 'th-list',
        path: '/arsip'
      },
      {
        name: 'Pemindahan Arsip',
        icon: 'archive',
        path: '/arsip/inaktif'
      }
    ]
  },
  {
    name: 'Pengaturan',
    icon: 'cog',
    path: '/config',
    allow: ['admin']
  },
  {
    name: 'Bantuan',
    icon: 'question-circle',
    path: '/help'
  }
]

const state = {
  isPending: false,
  sidebar: true,
  notifications: [],
}

const getters = {
  sidebarMenus: (state, getters, rootState) => {
    const userRoles = rootState.auth.user.roles
    return sidebarMenus.reduce((all, menu) => {
      if(menu.allow && !userRoles.some(role => ~menu.allow.indexOf(role))) return all
      all.push(menu)
      return all
    }, [])
  }
}

const mutations = {
  setPending(state, isPending) {
    state.isPending = isPending
  },
  setSidebar(state, sidebar) {
    state.sidebar = sidebar
  },
  addNotification(state, notification) {
    state.notifications.push(notification)
  },
  clearNotification(state) {
    state.notifications = []
  }
}

const actions = {
  startPending({ commit }) {
    NProgress.start()
    commit('setPending', true)
  },
  donePending({ commit }) {
    NProgress.done()
    commit('setPending', false)
  },
  newNotification({ commit }, {type, message}) {
    this._vm.$snotify[type](message)
    commit('addNotification', {type, message})
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}