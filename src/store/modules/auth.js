import router from '../../router'
import axios from '../../plugins/axios'

const state = {
  user: {
    idPegawai: '',
    name: '',
    nip: '',
    picture: '',
    roles: []
  },
  token: ''
}

const getters = {
  isLoggedIn: state => !!state.token
}

const mutations = {
  setUser(state, {idPegawai, name, nip, picture, roles}) {
    state.user.idPegawai = idPegawai
    state.user.name = name
    state.user.nip = nip
    state.user.picture = picture
    state.user.roles = roles
  },
  setToken(state, token) {
    state.token = token
  }
}

const actions = {
  loginKemenkeu({commit, dispatch}, {username, password}) {
    dispatch('startPending')
    axios.post('auth/kemenkeu', {username, password})
      .then(resp => resp.data)
      .then(data => {
        commit('setUser', data.user)
        commit('setToken', data.token)
        router.push('/')
      })
      .catch(err => {
        dispatch('newNotification', {type: 'error', message: err.message})
      })
      .finally(() => {
        dispatch('donePending')
      })
  },
  logoutKemenkeu({commit}) {
    commit('setUser', {
      idPegawai: '',
      name: '',
      nip: '',
      picture: '',
      roles: []
    })
    commit('setToken', '')
    router.push('/login')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}