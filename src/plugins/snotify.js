import Vue from 'vue'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import 'vue-snotify/styles/dark.css'

Vue.use(Snotify, {toast: {position: SnotifyPosition.rightTop}})
